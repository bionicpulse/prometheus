provider "azurerm" {
  features {}
}

resource "azurerm_resource_group" "default" {
  name     = var.cluster_name
  location = var.cluster_location
}

resource "azurerm_kubernetes_cluster" "default" {
  name                = var.cluster_name
  location            = var.cluster_location
  resource_group_name = azurerm_resource_group.default.name

  dns_prefix = var.cluster_name

  default_node_pool {
    name       = "default"
    node_count = 1
    vm_size    = "Standard_D2_v2"
  }

  monitor_metrics {
    annotations_allowed = var.metric_annotations_allowlist
    labels_allowed      = var.metric_labels_allowlist
  }

  identity {
    type = "SystemAssigned"
  }

  tags = {
    Environment = "Production"
  }
}

resource "azurerm_monitor_workspace" "default" {
  name                = var.cluster_name
  resource_group_name = azurerm_resource_group.default.name
  location            = var.cluster_location
  tags = {
    key = "value"
  }
}

resource "azurerm_dashboard_grafana" "default" {
  name                              = var.cluster_name
  resource_group_name               = azurerm_resource_group.default.name
  location                          = var.cluster_location
  api_key_enabled                   = true
  deterministic_outbound_ip_enabled = true
  public_network_access_enabled     = true

  identity {
    type = "SystemAssigned"
  }

  tags = {
    key = "value"
  }
}

#  azure_monitor_workspace_integrations {
#     resource_id  = var.monitor_workspace_id[var.monitor_workspace_id1, var.monitor_workspace_id2]
#   }

output "client_certificate" {
  value     = azurerm_kubernetes_cluster.default.kube_config.0.client_certificate
  sensitive = true
}

output "kube_config" {
  value = azurerm_kubernetes_cluster.default.kube_config_raw

  sensitive = true
}

